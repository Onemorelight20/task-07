package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String DB_URL;// = "jdbc:mysql://localhost:3306/testdb?user=root&password=root";
	private static final String INSERT_USER = "insert into users values (default, ?)";
	private static final String INSERT_TEAM = "insert into teams values (default, ?)";
	private static final String INSERT_USER_TEAM = "insert into users_teams values (?, ?)";
	private static final String SELECT_USERS = "select * from users";
	private static final String SELECT_TEAMS = "select * from teams";
	private static final String SELECT_USER_BY_LOGIN = "select * from users where login=?";
	private static final String SELECT_TEAM_BY_NAME = "select * from teams where name=?";
	private static final String SELECT_USER_TEAMS = "select team_id from users_teams where user_id=?";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	private static final String DELETE_USER = "DELETE FROM users WHERE login=?";
	private static final String UPDATE_TEAM_NAME_BY_ID = "UPDATE teams SET name=? where id =?";
	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null) {
		instance = new DBManager();
		}
		return instance;
	}
	
	static {
		String APP_PROPS_FILE = "app.properties";
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(APP_PROPS_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		DB_URL = properties.getProperty("connection.url");
	}


	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		Connection conn = null;
		Statement statement = null;
		List<User> users = new ArrayList<>();
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.createStatement();
			ResultSet result = statement.executeQuery(SELECT_USERS);
			while(result.next()) {
				User user = User.createUser(result.getString("login"));
				user.setId(result.getInt("id"));
				users.add(user);
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Failed to find all users", e);
		} finally {
			close(statement);
			close(conn);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			rs.next();
			int id = rs.getInt(1);
			user.setId(id);
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Failed to insert a user", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(DELETE_USER);
			for(int i = 0; i < users.length; i++) {
				statement.setString(1, users[i].getLogin());
				statement.executeUpdate();
			}
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("Failed to delete users", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public User getUser(String login) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(SELECT_USER_BY_LOGIN);
			statement.setString(1, login);
			ResultSet rs = statement.executeQuery();
			rs.next();
			User u = User.createUser(login);
			u.setId(rs.getInt(1));
			return u;
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get a user", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public Team getTeam(String name) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		Team team = null; 
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(SELECT_TEAM_BY_NAME);
			statement.setString(1, name);
			ResultSet rs = statement.executeQuery();
			rs.next();
			team = Team.createTeam(name);
			team.setId(rs.getInt(1));
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get team", e);
		} finally {
			close(statement);
			close(conn);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Connection conn = null;
		Statement statement = null;
		List<Team> teams = new ArrayList<>();
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.createStatement();
			ResultSet result = statement.executeQuery(SELECT_TEAMS);
			while(result.next()) {
				Team team = Team.createTeam(result.getString("name"));
				team.setId(result.getInt("id"));
				teams.add(team);
			}
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to find all teams", e);
		} finally {
			close(statement);
			close(conn);
		}
		return teams;
	}
	
	public boolean insertTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			statement.executeUpdate();
			ResultSet rs = statement.getGeneratedKeys();
			rs.next();
			int id = rs.getInt(1);
			team.setId(id);
			System.out.println("inserted" + team + " with id " + team.getId());
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to insert a team", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			conn.setAutoCommit(false);
			statement = conn.prepareStatement(INSERT_USER_TEAM);
			statement.setInt(1, user.getId());
			for(int i = 0; i < teams.length; i++) {
				statement.setInt(2, teams[i].getId());
				statement.executeUpdate();
			}
			
			conn.commit();
			return true;	
		} catch (Throwable e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
			throw new DBException("failed to set teams for user", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(SELECT_USER_TEAMS);
			statement.setInt(1, user.getId());
			List<Team> allTeams = this.findAllTeams();
			ResultSet rs = statement.executeQuery();
			while(rs.next()) {
				int teamId = rs.getInt(1);
				Team team = allTeams.stream().filter(x -> x.getId() == teamId).findFirst().orElse(null);
				if(team != null)
				teams.add(team);
			}
			
			return teams;	
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to get user teams", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(DELETE_TEAM);
			statement.setString(1, team.getName());
			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to delete a team", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement statement = null;
		
		try {
			conn = DriverManager.getConnection(DB_URL);
			statement = conn.prepareStatement(UPDATE_TEAM_NAME_BY_ID, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.executeUpdate();
			System.out.printf("the ream with name %s and id %d was updated%n", team.getName(), team.getId());
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("failed to update a team", e);
		} finally {
			close(statement);
			close(conn);
		}
	}

	
	private static void close(AutoCloseable ac) {
		if(ac != null) {
			try {
				ac.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
